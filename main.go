package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os/exec"
	"time"
)

var (
	commandLineHistory []string = []string{}
	tmpl               *template.Template
)

type templateInput struct {
	StandardOutput     string
	StandardError      string
	CommandLineHistory []string
	ExitError          error
}

type webPBTerminal struct{}

func getCommand(req *http.Request) string {
	return req.FormValue("commandlinecommandname")
}

func (w webPBTerminal) ServeHTTP(responseWriter http.ResponseWriter, req *http.Request) {
	req.ParseForm()

	latestCommand := getCommand(req)
	if latestCommand == "" {
		ourTemplateInput := templateInput{
			CommandLineHistory: commandLineHistory,
		}
		tmpl.Execute(responseWriter, ourTemplateInput)
		return
	}
	commandLineHistory = append(commandLineHistory, latestCommand)
	command := exec.Command("sh", "-c", latestCommand)
	stdout, err := command.StdoutPipe()
	if err != nil {
		fmt.Println("stdoutpipe", err)
		panic(err)
	}
	stderr, err := command.StderrPipe()
	if err != nil {
		fmt.Println("stderrpipe", err)
		panic(err)
	}
	if err = command.Start(); err != nil {
		fmt.Println("start", err)
		panic(err)
	}
	allstdout, err := ioutil.ReadAll(stdout)
	if err != nil {
		fmt.Println("readallstdout", err)
	}
	allstderr, err := ioutil.ReadAll(stderr)
	if err != nil {
		fmt.Println("readallstderr", err)
	}
	ourExitError := command.Wait()
	fmt.Println("allstdout: ", string(allstdout))
	fmt.Println("allstderr: ", string(allstderr))

	ourTemplateInput := templateInput{
		StandardOutput:     string(allstdout),
		StandardError:      string(allstderr),
		CommandLineHistory: commandLineHistory,
		ExitError:          ourExitError,
	}
	tmpl.Execute(responseWriter, ourTemplateInput)
}

func main() {
	var err error
	tmpl, err = template.New("index").Parse(indexHTML)
	if err != nil {
		fmt.Println("template.New", err)
	}

	myHandler := webPBTerminal{}

	s := &http.Server{
		Addr:           "127.0.0.1:9600",
		Handler:        myHandler,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	log.Fatal(s.ListenAndServe())
	time.Sleep(9999 * time.Second)
}

const indexHTML = `<!doctype HTML>
<html>
	<head>
		<meta charset="UTF-8"></meta>
		<title>WebPBTerm</title>
	</head>
	<body>
		<h1>WebPBTerm</h1>

		<pre>Stdout: {{.StandardOutput}}</pre>
		<pre>Stderr: {{.StandardError}}</pre>
		<pre>Exit Error: {{.ExitError}}</pre>

		<form action="" method="post">
			<label for="commandlinecommandid">Command Line Command:</label>
			<input id="commandlinecommandid" type="text" name="commandlinecommandname" required>
			<input type="Submit" value="Save">
		</form>
		{{range $k, $v := .CommandLineHistory}}
		<pre>{{$k}}: {{$v}}</pre>
		{{end}}
	</body>
</html>`
